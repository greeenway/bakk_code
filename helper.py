import time

START_KEYWORD = '[BEGIN] '
END_KEYWORD   = '[END]   '

class Timer(object):
    """The Timer class times the durations of several statements """
    def __init__(self, name=None):
        self.name = name


    def __enter__(self):
        phrase = START_KEYWORD
        if self.name:
            phrase += self.name

        print(phrase)
        self.tstart = time.time()


    def __exit__(self, type, value, traceback):
        phrase = END_KEYWORD
        if self.name:
            phrase += self.name
        print('{0:30} ({1:5.3f}ms)'.format(phrase, (time.time() - self.tstart)*1000))



if __name__ == '__main__':
    with Timer('processing'):
        blup = 2
    with Timer('drawing'):
        print("blup")
    with Timer():
            pass
    with Timer('drawing'):
        time.sleep(1)