#!/usr/local/bin/python3

import math

import preparation
import assembly
import solve
import plots


from helper import Timer

ELEMENTS_PER_LINE = 12
NODES_PER_ELEMENT = 4
STEP_SIZE = 0.2

def rho(x): return math.exp(-0.5/4**2*((x[0]-4)**2+(x[1]-12)**2))*1e-10-math.exp(-0.5/4**2*((x[0]-20)**2+(x[1]-12)**2))*1e-10
def eps(x): return 1+math.exp(-0.5/4**2*((x[1]-12)**2))*100

#data structure model
class Information:
    """Contains general information"""
    def __init__(self): 
        self.elements_per_line = ELEMENTS_PER_LINE;
        self.number_of_elements = int(math.pow(self.elements_per_line,2))
        self.nodes_per_element = NODES_PER_ELEMENT
        self.number_of_nodes = int(math.pow(self.elements_per_line+1,2))
        self.rho = rho
        self.eps = eps

class Tables:
    """Contains all needed tables"""
    def __init__(self):
        #self.element_table = None
        self.local_to_global = None
        self.nodes_to_coordinates = None
        self.boundary_table = None

class Plots:
    """Contains plot data"""
    def __init__(self):
        self.xx = None
        self.yy = None
        self.zz = None
        self.x_max = ELEMENTS_PER_LINE * 2
        self.y_max = ELEMENTS_PER_LINE * 2
        self.step = STEP_SIZE  #visualisation mesh refinement

class Equations:
    """Contains equation data"""
    def __init__(self):
        self.K_h = None
        self.f_h = None
        self.u_h = None


class Data:
    """Container structure to be filled with data"""
    def __init__(self):
        self.info = Information()
        self.tables = Tables()
        self.equ = None
        self.plots = Plots()
        

def run():
    data = Data()
    
    with Timer('preparation'):
        #create element_table
        #data.tables.element_table = preparation.create_element_table(data.info) #unused?

        #create local_to_global table
        data.tables.local_to_global = preparation.create_local_to_global(data.info)

        #create nodes_to_coordinates
        data.tables.nodes_to_coordinates = preparation.create_nodes_to_coordinates(data.info)

        #create boundary_table
        data.tables.boundary_table = preparation.create_boundary_table(data.info)

    with Timer('assembly'):
        #assemble equations
        data.equ = assembly.assemble_equations(data.info, data.tables)

    with Timer('solve'):
        #solve equations
        data.equ.u_h = solve.solve(data.equ)
        

    with Timer('plots'):
        #generate plot_data
        data.plots.xx, data.plots.yy, data.plots.zz = plots.generate_plot_data(data.info, 
                                    data.tables, data.plots, data.equ.u_h)

        plots.contour_plot(data.info, data.tables, data.plots, data.equ.u_h)
        
        plots.fe_plot(data.info, data.tables, data.plots)



#execute run function when executed
if __name__ == '__main__':
    run()








